EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+12V #PWR05
U 1 1 5FCF7D3A
P 5000 2700
F 0 "#PWR05" H 5000 2550 50  0001 C CNN
F 1 "+12V" H 5015 2873 50  0000 C CNN
F 2 "" H 5000 2700 50  0001 C CNN
F 3 "" H 5000 2700 50  0001 C CNN
	1    5000 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR06
U 1 1 5FCF9F2A
P 5000 4650
F 0 "#PWR06" H 5000 4400 50  0001 C CNN
F 1 "GNDD" H 5004 4495 50  0000 C CNN
F 2 "" H 5000 4650 50  0001 C CNN
F 3 "" H 5000 4650 50  0001 C CNN
	1    5000 4650
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP627 U1
U 1 1 5FCFC8E6
P 3150 3850
F 0 "U1" H 3150 4175 50  0000 C CNN
F 1 "TLP627" H 3150 4084 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 2850 3650 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=16914&prodName=TLP627" H 3150 3850 50  0001 L CNN
	1    3150 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4350 5000 4650
$Comp
L power:GNDD #PWR01
U 1 1 5FD23FE1
P 3550 4650
F 0 "#PWR01" H 3550 4400 50  0001 C CNN
F 1 "GNDD" H 3554 4495 50  0000 C CNN
F 2 "" H 3550 4650 50  0001 C CNN
F 3 "" H 3550 4650 50  0001 C CNN
	1    3550 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3950 3550 4650
$Comp
L power:+12V #PWR02
U 1 1 5FD28172
P 4000 2700
F 0 "#PWR02" H 4000 2550 50  0001 C CNN
F 1 "+12V" H 4015 2873 50  0000 C CNN
F 2 "" H 4000 2700 50  0001 C CNN
F 3 "" H 4000 2700 50  0001 C CNN
	1    4000 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5FD29D38
P 4000 3200
F 0 "R2" H 4070 3246 50  0000 L CNN
F 1 "100k" H 4070 3155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3930 3200 50  0001 C CNN
F 3 "~" H 4000 3200 50  0001 C CNN
	1    4000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3550 5000 3450
$Comp
L Device:R R3
U 1 1 5FD40C7F
P 4350 3200
F 0 "R3" H 4420 3246 50  0000 L CNN
F 1 "100k" H 4420 3155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4280 3200 50  0001 C CNN
F 3 "~" H 4350 3200 50  0001 C CNN
	1    4350 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR03
U 1 1 5FD425C8
P 4350 2700
F 0 "#PWR03" H 4350 2550 50  0001 C CNN
F 1 "+12V" H 4365 2873 50  0000 C CNN
F 2 "" H 4350 2700 50  0001 C CNN
F 3 "" H 4350 2700 50  0001 C CNN
	1    4350 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4150 4350 4150
Wire Wire Line
	4350 4150 4350 3350
Wire Wire Line
	4000 2700 4000 3050
Wire Wire Line
	4350 2700 4350 3050
Wire Wire Line
	4000 3350 4000 3750
Connection ~ 4000 3750
Wire Wire Line
	4000 3750 4500 3750
$Comp
L Switch:SW_Push SW1
U 1 1 5FD44100
P 4350 4350
F 0 "SW1" V 4396 4497 50  0000 L CNN
F 1 "Rücksetzen" V 4305 4497 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 4350 4550 50  0001 C CNN
F 3 "~" H 4350 4550 50  0001 C CNN
	1    4350 4350
	0    -1   -1   0   
$EndComp
Connection ~ 4350 4150
$Comp
L power:GNDD #PWR04
U 1 1 5FD45F0B
P 4350 4650
F 0 "#PWR04" H 4350 4400 50  0001 C CNN
F 1 "GNDD" H 4354 4495 50  0000 C CNN
F 2 "" H 4350 4650 50  0001 C CNN
F 3 "" H 4350 4650 50  0001 C CNN
	1    4350 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 4550 4350 4650
$Comp
L Timer:ICM7556 U3
U 1 1 5FCF547C
P 5000 3950
F 0 "U3" H 4900 4000 50  0000 L CNN
F 1 "ICM7556" H 4800 3900 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5000 3950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/icm7/icm7555-56.pdf" H 5000 3950 50  0001 C CNN
	1    5000 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5FD6AE94
P 2100 3050
F 0 "R1" H 2170 3096 50  0000 L CNN
F 1 "22k" H 2170 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2030 3050 50  0001 C CNN
F 3 "~" H 2100 3050 50  0001 C CNN
	1    2100 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 2750 2100 2900
Wire Wire Line
	1500 2500 1500 2650
$Comp
L Diode:1N4148 D3
U 1 1 5FD858A4
P 2500 3850
F 0 "D3" H 2500 3633 50  0000 C CNN
F 1 "1N4148" H 2500 3724 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2500 3675 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 2500 3850 50  0001 C CNN
	1    2500 3850
	0    1    1    0   
$EndComp
$Comp
L Diode:1N47xxA D2
U 1 1 5FD8D29F
P 1500 2800
F 0 "D2" V 1546 2720 50  0000 R CNN
F 1 "BZX79-C 24" V 1455 2720 50  0000 R CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1500 2625 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 1500 2800 50  0001 C CNN
	1    1500 2800
	0    -1   -1   0   
$EndComp
$Comp
L Regulator_Linear:L78L12_TO92 U2
U 1 1 5FD972C4
P 4500 1250
F 0 "U2" H 4500 1492 50  0000 C CNN
F 1 "L78L12" H 4500 1401 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4500 1475 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/15/55/e5/aa/23/5b/43/fd/CD00000446.pdf/files/CD00000446.pdf/jcr:content/translations/en.CD00000446.pdf" H 4500 1200 50  0001 C CNN
	1    4500 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1250 4850 1250
$Comp
L power:+12V #PWR08
U 1 1 5FD9BF96
P 5700 1250
F 0 "#PWR08" H 5700 1100 50  0001 C CNN
F 1 "+12V" V 5715 1378 50  0000 L CNN
F 2 "" H 5700 1250 50  0001 C CNN
F 3 "" H 5700 1250 50  0001 C CNN
	1    5700 1250
	0    1    1    0   
$EndComp
$Comp
L power:GNDD #PWR09
U 1 1 5FD9F2CF
P 5700 2150
F 0 "#PWR09" H 5700 1900 50  0001 C CNN
F 1 "GNDD" V 5704 2040 50  0000 R CNN
F 2 "" H 5700 2150 50  0001 C CNN
F 3 "" H 5700 2150 50  0001 C CNN
	1    5700 2150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4500 1550 4500 2150
Connection ~ 4500 2150
Wire Wire Line
	4500 2150 4850 2150
$Comp
L power:GNDD #PWR07
U 1 1 5FDA73E8
P 5500 4650
F 0 "#PWR07" H 5500 4400 50  0001 C CNN
F 1 "GNDD" H 5504 4495 50  0000 C CNN
F 2 "" H 5500 4650 50  0001 C CNN
F 3 "" H 5500 4650 50  0001 C CNN
	1    5500 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 4150 5500 4650
$Comp
L Device:CP C2
U 1 1 5FCF9D9F
P 3700 1700
F 0 "C2" H 3818 1746 50  0000 L CNN
F 1 "100µ" H 3818 1655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L18.0mm_D8.0mm_P25.00mm_Horizontal" H 3738 1550 50  0001 C CNN
F 3 "~" H 3700 1700 50  0001 C CNN
	1    3700 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5FCFABEA
P 4850 1700
F 0 "C4" H 4965 1746 50  0000 L CNN
F 1 "100n" H 4965 1655 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D8.0mm_W2.5mm_P5.00mm" H 4888 1550 50  0001 C CNN
F 3 "~" H 4850 1700 50  0001 C CNN
	1    4850 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5FCFE1B3
P 4150 1700
F 0 "C3" H 4265 1746 50  0000 L CNN
F 1 "100n" H 4265 1655 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D8.0mm_W2.5mm_P5.00mm" H 4188 1550 50  0001 C CNN
F 3 "~" H 4150 1700 50  0001 C CNN
	1    4150 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1550 4150 1250
Connection ~ 4150 1250
Wire Wire Line
	4150 1250 4200 1250
Wire Wire Line
	4150 1850 4150 2150
Connection ~ 4150 2150
Wire Wire Line
	4150 2150 4500 2150
Wire Wire Line
	4850 1550 4850 1250
Connection ~ 4850 1250
Wire Wire Line
	4850 1250 5700 1250
Wire Wire Line
	4850 1850 4850 2150
Connection ~ 4850 2150
Wire Wire Line
	4850 2150 5700 2150
Wire Wire Line
	2100 3200 2100 3600
Wire Wire Line
	2100 3600 2500 3600
Wire Wire Line
	2500 3600 2500 3700
Wire Wire Line
	1500 2950 1500 4100
Wire Wire Line
	1500 4100 2500 4100
Wire Wire Line
	2500 4100 2500 4000
Wire Wire Line
	2500 3600 2750 3600
Wire Wire Line
	2750 3600 2750 3750
Connection ~ 2500 3600
Wire Wire Line
	2500 4100 2750 4100
Wire Wire Line
	2750 4100 2750 3950
Connection ~ 2500 4100
$Comp
L Diode:1N4148 D4
U 1 1 5FD26506
P 2700 1500
F 0 "D4" H 2700 1283 50  0000 C CNN
F 1 "1N4148" H 2700 1374 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2700 1325 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 2700 1500 50  0001 C CNN
	1    2700 1500
	0    1    1    0   
$EndComp
$Comp
L Diode:1N4148 D6
U 1 1 5FD28F51
P 3100 1500
F 0 "D6" H 3110 1730 50  0000 C CNN
F 1 "1N4148" H 3100 1630 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3100 1325 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3100 1500 50  0001 C CNN
	1    3100 1500
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D7
U 1 1 5FD36CAB
P 3100 1900
F 0 "D7" H 3100 1683 50  0000 C CNN
F 1 "1N4148" H 3100 1774 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3100 1725 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3100 1900 50  0001 C CNN
	1    3100 1900
	0    1    1    0   
$EndComp
$Comp
L Diode:1N4148 D5
U 1 1 5FD33242
P 2700 1900
F 0 "D5" H 2700 2120 50  0000 C CNN
F 1 "1N4148" H 2700 2030 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2700 1725 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 2700 1900 50  0001 C CNN
	1    2700 1900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2700 2050 3100 2050
Wire Wire Line
	2700 1350 3100 1350
Wire Wire Line
	3100 1650 3100 1700
Wire Wire Line
	2700 1650 2700 1700
Wire Wire Line
	3100 1700 3400 1700
Wire Wire Line
	3400 1700 3400 1250
Connection ~ 3100 1700
Wire Wire Line
	3100 1700 3100 1750
Wire Wire Line
	2700 1700 2400 1700
Wire Wire Line
	2400 1700 2400 2150
Connection ~ 2700 1700
Wire Wire Line
	2700 1700 2700 1750
Wire Wire Line
	3550 3750 4000 3750
Wire Wire Line
	7150 2150 7150 2250
Wire Wire Line
	7150 2950 7150 3050
Wire Wire Line
	7150 2550 7150 2650
Wire Wire Line
	7150 3350 7150 3450
Wire Wire Line
	6400 4650 6400 4350
$Comp
L power:+12V #PWR011
U 1 1 5FD9E91F
P 6400 2700
F 0 "#PWR011" H 6400 2550 50  0001 C CNN
F 1 "+12V" H 6415 2873 50  0000 C CNN
F 2 "" H 6400 2700 50  0001 C CNN
F 3 "" H 6400 2700 50  0001 C CNN
	1    6400 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR012
U 1 1 5FD9DA9B
P 6400 4650
F 0 "#PWR012" H 6400 4400 50  0001 C CNN
F 1 "GNDD" H 6404 4495 50  0000 C CNN
F 2 "" H 6400 4650 50  0001 C CNN
F 3 "" H 6400 4650 50  0001 C CNN
	1    6400 4650
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR013
U 1 1 5FD56D6E
P 7150 2150
F 0 "#PWR013" H 7150 2000 50  0001 C CNN
F 1 "+12V" H 7165 2323 50  0000 C CNN
F 2 "" H 7150 2150 50  0001 C CNN
F 3 "" H 7150 2150 50  0001 C CNN
	1    7150 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5FD55FFE
P 7150 2400
F 0 "R4" H 7220 2446 50  0000 L CNN
F 1 "1k2" H 7220 2355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7080 2400 50  0001 C CNN
F 3 "~" H 7150 2400 50  0001 C CNN
	1    7150 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D8
U 1 1 5FD53A28
P 7150 2800
F 0 "D8" V 7097 2682 50  0000 R CNN
F 1 "rot" V 7188 2682 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 7150 2800 50  0001 C CNN
F 3 "~" H 7150 2800 50  0001 C CNN
	1    7150 2800
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D9
U 1 1 5FD52DAE
P 7150 3200
F 0 "D9" V 7097 3082 50  0000 R CNN
F 1 "rot" V 7188 3082 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 7150 3200 50  0001 C CNN
F 3 "~" H 7150 3200 50  0001 C CNN
	1    7150 3200
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D10
U 1 1 5FD4FBAD
P 7150 3600
F 0 "D10" V 7097 3482 50  0000 R CNN
F 1 "rot" V 7188 3482 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 7150 3600 50  0001 C CNN
F 3 "~" H 7150 3600 50  0001 C CNN
	1    7150 3600
	0    1    -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5FD4DBE6
P 7150 4100
F 0 "R5" H 7220 4146 50  0000 L CNN
F 1 "6M8" H 7220 4055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7080 4100 50  0001 C CNN
F 3 "~" H 7150 4100 50  0001 C CNN
	1    7150 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 4150 6900 4150
Wire Wire Line
	5800 4400 7000 4400
Wire Wire Line
	5900 3750 5800 3750
Wire Wire Line
	5650 4150 5900 4150
$Comp
L Device:C C5
U 1 1 5FD496BD
P 5800 3000
F 0 "C5" H 5915 3046 50  0000 L CNN
F 1 "100n" H 5915 2955 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 5838 2850 50  0001 C CNN
F 3 "~" H 5800 3000 50  0001 C CNN
	1    5800 3000
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR010
U 1 1 5FD4A14F
P 5800 2700
F 0 "#PWR010" H 5800 2550 50  0001 C CNN
F 1 "+12V" H 5815 2873 50  0000 C CNN
F 2 "" H 5800 2700 50  0001 C CNN
F 3 "" H 5800 2700 50  0001 C CNN
	1    5800 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2700 5800 2850
Connection ~ 5800 3750
Wire Wire Line
	5500 3750 5650 3750
Wire Wire Line
	5650 3750 5650 4150
Wire Wire Line
	5800 3150 5800 3750
Wire Wire Line
	5800 3750 5800 4400
$Comp
L Timer:ICM7556 U3
U 2 1 5FCF6BBD
P 6400 3950
F 0 "U3" H 6300 4000 50  0000 L CNN
F 1 "ICM7556" H 6200 3900 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6400 3950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/icm7/icm7555-56.pdf" H 6400 3950 50  0001 C CNN
	2    6400 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 2700 6400 3550
Wire Wire Line
	3400 1250 3700 1250
Wire Wire Line
	2400 2150 3700 2150
Wire Wire Line
	3700 1850 3700 2150
Connection ~ 3700 2150
Wire Wire Line
	3700 2150 4150 2150
Wire Wire Line
	3700 1550 3700 1250
Connection ~ 3700 1250
Wire Wire Line
	3700 1250 4150 1250
$Comp
L Device:C C6
U 1 1 5FD916B7
P 5250 3450
F 0 "C6" H 5365 3496 50  0000 L CNN
F 1 "100n" H 5365 3405 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D8.0mm_W2.5mm_P5.00mm" H 5288 3300 50  0001 C CNN
F 3 "~" H 5250 3450 50  0001 C CNN
	1    5250 3450
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDD #PWR0101
U 1 1 5FD93BA8
P 5550 3450
F 0 "#PWR0101" H 5550 3200 50  0001 C CNN
F 1 "GNDD" H 5554 3295 50  0000 C CNN
F 2 "" H 5550 3450 50  0001 C CNN
F 3 "" H 5550 3450 50  0001 C CNN
	1    5550 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5100 3450 5000 3450
Connection ~ 5000 3450
Wire Wire Line
	5000 3450 5000 2700
Wire Wire Line
	5400 3450 5550 3450
Connection ~ 2700 1350
Connection ~ 2700 2050
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 5FD1B0B0
P 1100 2050
F 0 "J2" H 1100 1950 50  0000 C CNN
F 1 "a" H 1100 2150 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x01_P2.54mm_Vertical" H 1100 2050 50  0001 C CNN
F 3 "~" H 1100 2050 50  0001 C CNN
	1    1100 2050
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J1
U 1 1 5FD16DB7
P 1100 1350
F 0 "J1" H 1100 1250 50  0000 C CNN
F 1 "b" H 1100 1450 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x01_P2.54mm_Vertical" H 1100 1350 50  0001 C CNN
F 3 "~" H 1100 1350 50  0001 C CNN
	1    1100 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	2100 1350 2700 1350
Connection ~ 2100 1350
Wire Wire Line
	2100 2450 2100 1350
Wire Wire Line
	1300 1350 2100 1350
Connection ~ 1500 2050
Wire Wire Line
	1500 2050 2700 2050
Wire Wire Line
	1500 2050 1500 2200
$Comp
L Diode:1N47xxA D1
U 1 1 5FD904DB
P 1500 2350
F 0 "D1" V 1454 2430 50  0000 L CNN
F 1 "BZX79-C 24" V 1545 2430 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1500 2175 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 1500 2350 50  0001 C CNN
	1    1500 2350
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5FD69EA5
P 2100 2600
F 0 "C1" H 2215 2646 50  0000 L CNN
F 1 "100n" H 2215 2555 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 2138 2450 50  0001 C CNN
F 3 "~" H 2100 2600 50  0001 C CNN
	1    2100 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2050 1500 2050
Wire Wire Line
	6900 3750 7150 3750
Wire Wire Line
	7000 4150 7000 4250
Wire Wire Line
	7150 4250 7000 4250
Connection ~ 7000 4250
Wire Wire Line
	7000 4250 7000 4400
Wire Wire Line
	7150 3950 7150 3750
Connection ~ 7150 3750
$EndSCHEMATC
